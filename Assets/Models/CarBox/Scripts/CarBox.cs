using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarBox : MonoBehaviour
{
    public CarLights car;
    public Animator fallAnimator;
    public Animator boxOpenAnimator;
    public Rigidbody coverRigidBody;

    public System.Action onCarShowed;
    private void OnEnable()
    {
        var carBoxOpen = boxOpenAnimator.GetComponent<CarBoxOpen>();
        if(carBoxOpen != null)
        {
            carBoxOpen.onOpenAnimationFinished += carBoxOpenAnimationFinished;
        }
    }
    private void OnDisable()
    {
        var carBoxOpen = boxOpenAnimator.GetComponent<CarBoxOpen>();
        if (carBoxOpen != null)
        {
            carBoxOpen.onOpenAnimationFinished -= carBoxOpenAnimationFinished;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void startAnimation()
    {
        StartCoroutine(delayStartFallAnimation());
    }
    IEnumerator delayStartFallAnimation()
    {
        yield return new WaitForSeconds(1);
        fallAnimator.enabled = true;
    }
    public void fallAnimationFinished()
    {
        StartCoroutine(delayOpenBox());
    }
    IEnumerator delayOpenBox()
    {
        yield return new WaitForSeconds(1);
        coverRigidBody.AddForce(0, 100, -150, ForceMode.Impulse);

        yield return new WaitForSeconds(1.0f);
        this.GetComponent<AudioSource>().Play();
        boxOpenAnimator.enabled = true;
        yield return new WaitForSeconds(10f);
        coverRigidBody.gameObject.SetActive(false);
    }
    void carBoxOpenAnimationFinished()
    {
        onCarShowed?.Invoke();
    }
    public void startCarAnimation()
    {
        car.enabled = true;
        var carAnimator = car.GetComponent<Animator>();
        if (carAnimator != null)
        {
            carAnimator.enabled = true;
        }
    }
}
