﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    public ParticleSystem particle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void mineFinishedAnim()
    {
        this.gameObject.SetActive(false);
        particle.gameObject.SetActive(true);
        SceneLogic.instance.shakeCamera();
    }
}
