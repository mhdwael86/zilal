﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataParser : MonoBehaviour
{
    public string fileName;

    private string path;
    private  Zilal.Data currentData;
    //private List<Zilal.Action> actionsQueue = new List<Zilal.Action>();
    private bool isActionInProgress = false;
    public System.Action callback;
    private List<Zilal.Action> actionsQueue;

    private float readDelay = 0.5f;

    private void Awake()
    {
        path = Path.Combine(Application.streamingAssetsPath, fileName) + ".json";
    }
    private void OnEnable()
    {
        SceneManager.sceneLoaded += onSceneLoaded;
    }
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= onSceneLoaded;
    }
    // Start is called before the first frame update
    void Start()
    {
        initializeGame();
    }
    void onSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex != 1)
            return;
        //initializeGame();
    }
    void initializeGame()
    {
        actionsQueue = GameManager.instance.actionsQueue;
        isActionInProgress = false;
        read();
        StartCoroutine(tick());
        SceneLogic.instance.callback = actionFinished;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator tick()
    {
        yield return new WaitForSeconds(readDelay);
        read();
        StartCoroutine(tick());
    }
    private void read()
    {
        try
        {
            string contents = File.ReadAllText(path);
            currentData = JsonUtility.FromJson<Zilal.Data>(contents);
        }
        catch
        {
            Debug.LogWarning("Wrong json file");
        }
        parse();
    }
    private void parse()
    {
        if (currentData == null)
        {
            Debug.Log("current json data is null");
            return;
        }
        for (int i = 0; i < currentData.Actions.Length; i++)
        {
            if (currentData.Actions[i].actionId > GameManager.instance.lastReadActionId)
            {
                GameManager.instance.lastReadActionId = currentData.Actions[i].actionId;
                if (currentData.Actions[i].type == Zilal.ActionType.TIMER_ACTION)
                {
                    parseTimer(currentData.Actions[i]);
                    continue;
                }
                actionsQueue.Add(currentData.Actions[i]);
                
                Debug.Log("Add Action: " + currentData.Actions[i].type);
            }
        }
        if(!isActionInProgress)
        {
            if (actionsQueue.Count > 0)
            {
                isActionInProgress = true;
                parseAction(actionsQueue[0]);
            }
        }
    }
    void parseAction(Zilal.Action action)
    {
        Debug.Log("Execute Action: " + action.type);

        Debug.Log("Execute Action Json : " + JsonUtility.ToJson(action));
        switch (action.type)
        {
            case Zilal.ActionType.INITIALIZE:
                SceneLogic.instance.intiailize(action);
                break;
            case Zilal.ActionType.RESET:
                execReset(action);
                break;
            case Zilal.ActionType.OPEN_BOX:
                execOpenBox(action);
                break;
            case Zilal.ActionType.SELECT_ANSWER:
                SceneLogic.instance.selectAnswer(action);
                break;
            case Zilal.ActionType.SET_USER_INFO:
                SceneLogic.instance.setUserInfo(action);
                break;
            case Zilal.ActionType.BACK_TO_GAME:
                SceneLogic.instance.backToGame();
                break;
            case Zilal.ActionType.BALANCE_UPDATE:
                SceneLogic.instance.updateBalance(action);
                break;
            case Zilal.ActionType.FREEZEBALANCE:
                SceneLogic.instance.setFrozenBalance(action);
                break;
            case Zilal.ActionType.DOUBLE_BALANCE:
                SceneLogic.instance.changeDoubleBalance(action);
                break;
            case Zilal.ActionType.GAME_OVER:
                SceneLogic.instance.loseGame(action);
                break;
            case Zilal.ActionType.GAME_WIN:
                SceneLogic.instance.winGame(action);
                break;
            case Zilal.ActionType.SHOW_DIALOG:
                SceneLogic.instance.showResult(action.balance);
                break;
            case Zilal.ActionType.HIDE_DIALOG:
                SceneLogic.instance.hideResult(action.balance);
                break;
            case Zilal.ActionType.RETREAT_GAME:
                SceneLogic.instance.showResult(action.reward.value);
                break;
            case Zilal.ActionType.WIN_CAR:
                SceneLogic.instance.winCar(action);
                break;
                case "delay":
                SceneLogic.instance.defaultCallback(action.duration);
                break;
            case Zilal.ActionType.SHOW_VIDEO:
                SceneLogic.instance.showVideo();
                break;
            case Zilal.ActionType.HIDE_VIDEO:
                SceneLogic.instance.hideVideo();
                break;
            case Zilal.ActionType.SHOW_NUMBER:
                SceneLogic.instance.showNumber();
                break;
            case Zilal.ActionType.HIDE_NUMBER:
                SceneLogic.instance.hideNumber();
                break;
            case Zilal.ActionType.WIN_NUMBER:
                SceneLogic.instance.winNumber(action);
                break;
            default:
                actionFinished();
                break;
        }
    }
    void changeAction()
    {

    }
    void parseTimer(Zilal.Action action)
    {
        Debug.Log("Parse Timer: " + action.action);
        switch (action.action)
        {
            case Zilal.ActionType.TIMER_PAUSE:
                SceneLogic.instance.hud.pauseTimer(action.time);
                break;
            case Zilal.ActionType.TIMER_PLAY:
                SceneLogic.instance.hud.startTimer(action.time);
                break;
            case Zilal.ActionType.TIMER_SET_TIME:
                SceneLogic.instance.hud.setTimer(action.time);
                break;
            default:
                break;
        }
    }
    void execReset(Zilal.Action action)
    {
        Debug.Log("execReset");
        StopCoroutine(tick());
        
        if (actionsQueue.Count > 0)
        {
            Debug.Log("remove action from queue after reset");
            actionsQueue.Remove(actionsQueue[0]);
        }
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        SceneLogic.instance.resetApp();
    }
    void execOpenBox(Zilal.Action action)
    {
        switch (action.boxType)
        {
            case "bomb":
                SceneLogic.instance.showBomb(action);
                break;
            case "normal":
                SceneLogic.instance.showNormal(action);
                break;
            case "question":
                SceneLogic.instance.showQuestion(action);
                break;
            case "tresure":
                SceneLogic.instance.showTresure(action);
                break;
            default:
                break;
        }
    }
    void actionFinished()
    {
        Debug.Log("actionFinished: " + actionsQueue.Count);
        if (actionsQueue.Count > 0)
        {
            Debug.Log("Remove Action: id: " + actionsQueue[0].actionId + " actionType: " + actionsQueue[0].type);
            actionsQueue.RemoveAt(0);
        }

        isActionInProgress = false;
        
        parse();
    }
}

