﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hud : MonoBehaviour
{
    public RTLTMPro.RTLTextMeshPro nameValue;
    public RTLTMPro.RTLTextMeshPro cashValue;
    public RTLTMPro.RTLTextMeshPro frozenBalance;
    public UnityEngine.UI.Text timerText;
    public UnityEngine.UI.Image timerBar;
    //public RTLTMPro.RTLTextMeshPro doubleBalance;
    public UnityEngine.UI.Text doubleBalance;
    public Color timerBarNormalColor;
    public Color timerBarAlomstEndColor;
    public Animator doubleBg;

    float currentTime = 0;
    bool isTimeActive = false;
    public int CurrentBalance
    {
        get;
        set;
    }

    public void intialize()
    {
        timerText.gameObject.SetActive(false);
        nameValue.text = "اسم المتسابق";
        cashValue.text = "0";
        frozenBalance.text = "0";
        doubleBalance.text = "";
        doubleBg.gameObject.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        timerBar.color = timerBarNormalColor;
        isTimeActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(isTimeActive)
        {
            currentTime = currentTime - Time.deltaTime;
            Debug.Log("current time " + currentTime + "  " + Time.deltaTime);
            if (currentTime < 0)
                currentTime = 0;
            updateTimer((int)currentTime);
        }
    }

    public void setBalance(int value)
    {
        CurrentBalance = value;
        cashValue.text = SceneLogic.instance.balanceToString(value);
    }
    public void setName(string name)
    {
        nameValue.text = name;
    }
    public void setFrozenBalance(int value)
    {
        frozenBalance.text = SceneLogic.instance.balanceToString(value);
    }
    public void setDoubleBalance(int value)
    {
        if (value == 1)
        {
            doubleBg.gameObject.SetActive(false);
            doubleBalance.text = "";
            return;
        }

        doubleBg.gameObject.SetActive(true);
        doubleBalance.text = "X" + value;
        doubleBg.SetTrigger("show");
    }

    #region Timer
    public void startTimer(int value)
    {
        isTimeActive = true;
        currentTime = value;
        updateTimer(value);
        Debug.Log("start timer " + currentTime + "  " + GameManager.instance.totalTime);
    }
    public void pauseTimer(int value)
    {
        isTimeActive = false;
        currentTime = value;
        updateTimer(value);
    }
    public void setTimer(int value)
    {
        currentTime = value;
        updateTimer(value);
    }
    private void updateTimer(int value)
    {
        timerText.text = value.ToString();
        var totalTime = GameManager.instance.totalTime;
        timerBar.fillAmount =  1 - ((float)(totalTime - value) / (float)totalTime);
        if(value < 10)
            timerBar.color = timerBarAlomstEndColor;
        else
            timerBar.color = timerBarNormalColor;
        /*timerText.color = Color.white;
        if (value <= 3)
            timerText.color = Color.red;*/
    }
    #endregion
}
