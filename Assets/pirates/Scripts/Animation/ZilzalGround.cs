﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZilzalGround : MonoBehaviour
{
    public ParticleSystem moneyParticle;
    public Animator animator;
    private void Awake()
    {
        animator.enabled = false;
        hideParticle();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void playZilzalSFX()
    {
        this.GetComponent<AudioSource>().Play();
    }
    public void stopZilzalSFX()
    {
        this.GetComponent<AudioSource>().Stop();
    }
    public void playZilzalAnimation()
    {
        animator.enabled = true;
    }
    public void hideParticle()
    {
        moneyParticle.gameObject.SetActive(false);
        moneyParticle.Stop();
    }
    public void showParticle()
    {
        moneyParticle.gameObject.SetActive(true);
        moneyParticle.Play();
    }
}
