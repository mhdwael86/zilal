﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationCallback : MonoBehaviour
{
    public System.Action animationEndCallback;
    public System.Action animationOpenCallback;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onAnimationEnd()
    {
        animationEndCallback?.Invoke();
    }
    public void onAnimationOpen()
    {
        animationOpenCallback?.Invoke();
    }
}
