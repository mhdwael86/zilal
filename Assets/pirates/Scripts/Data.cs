﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Zilal
{
    [Serializable]
    public class Target
    {
        public int row;
        public int col;
    }
    [Serializable]
    public class Reward
    {
        public string type;
        public int value;
    }
    [Serializable]
    public class Question
    {
        public string text;
        public string image;
        public string ans1;
        public string ans2;
        public string ans3;
        public string ans4;
        public int correctAnswer;
    }
    [Serializable]
    public class Action
    {
        public string username;
        public long actionId;
        public int gridWidth = 6;
        public int gridHeight = 6;
        public string type;
        public Target target;
        public Target[] affectedBoxes;
        public bool showQuestion = false;
        public Reward reward;
        public Question question;
        public int selectedAnswer;
        public int correctAnswer;
        public string boxType;
        public int balance;
        public int frozenBalance;
        public int doubleBalance;

        public string action;
        public int time;
        public int totalTime;

        public float duration = 1;

        public string number = "";
    }
    [Serializable]
    public class Data
    {
        public Action[] Actions;
    }

    public class ActionType
    {
        public const string TIMER_ACTION = "timerAction";
        public const string INITIALIZE = "initialize";
        public const string GAME_OVER = "lose";
        public const string GAME_WIN = "win";
        public const string WIN_CAR = "winCar";
        public const string SET_USER_INFO = "setUserInfo";
        public const string OPEN_BOX = "openBox";
        public const string SELECT_ANSWER = "selectAnswer";
        public const string RESET = "reset";
        public const string BACK_TO_GAME = "backToGame";
        public const string BALANCE_UPDATE = "balanceUpdate";
        public const string FREEZEBALANCE = "freezeBalance";
        public const string DOUBLE_BALANCE = "changeDoubleBalance";
        public const string BOX_TYPE_TRESURE = "tresure";
        public const string BOX_TYPE_BOMB = "bomb";
        public const string REWARD_CASH = "cash";

        public const string TIMER_PLAY = "play";
        public const string TIMER_PAUSE = "pause";
        public const string TIMER_SET_TIME = "setTime";
        public const string BOX_TYPE_NORMAL = "normal";


        public const string SHOW_DIALOG = "showDialog";
        public const string HIDE_DIALOG = "hideDialog";
        public const string RETREAT_GAME = "retreat";


        public const string SHOW_VIDEO = "showVideo";
        public const string HIDE_VIDEO = "hideVideo";

        public const string SHOW_NUMBER = "showNumber";
        public const string HIDE_NUMBER = "hideNumber";
        public const string WIN_NUMBER = "winNumber";
    }
    public class TextKeys
    {
        public const string RESULT_WIN = "مبروووك";
        public const string RESULT_LOSE = "حظا أوفر";
    }
}
