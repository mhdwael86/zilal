﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CubePrefab : MonoBehaviour
{
    public GameObject tresure;
    public GameObject mine;
    public GameObject questionMark;
    public GameObject cubeAnimationHelper;

    //public RTLTMPro.RTLTextMeshPro3D treasureHud;
    public GameObject treasureHud;
    public RTLTMPro.RTLTextMeshPro3D questionHud;
    public GameObject danadesh;
    //public GameObject tresureHudObject;
    private Vector3 originalTresurePos;
    private Vector3 screenPosition;
    bool canMove = false;
    bool canScale = false;
    public Animator popupAnimator;
    public TextMesh number;
    //new Vector3(403.6f, 308.2f, 0.8f)
    //874 491

    // Start is called before the first frame update
    void Start()
    {
        //screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2.0f, Screen.height / 2.0f, 0.8f));
        screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2.0f, Screen.height / 2.0f, Camera.main.nearClipPlane));
        originalTresurePos = treasureHud.transform.position;
        popupAnimator.GetComponent<Danadesh>().animationCallback = onfadeIn;
    }
    void onfadeIn()
    {
        scaleCallback();
    }

    // Update is called once per frame
    void Update()
    {
        if(canMove)
        {
            treasureHud.transform.position = Vector3.MoveTowards(treasureHud.transform.position, originalTresurePos, Time.deltaTime * 8.0f);
            var distance = Vector3.Distance(treasureHud.transform.position, originalTresurePos);
            if (distance < 0.01f)
            {
                treasureHud.transform.position = originalTresurePos;
                canMove = false;
                //danadesh.gameObject.SetActive(false);
                //scaleCallback();
            }
        }
        if(canScale)
        {
            treasureHud.transform.position = Vector3.MoveTowards(treasureHud.transform.position, screenPosition, Time.deltaTime * 8.0f);
            var distance = Vector3.Distance(treasureHud.transform.position, screenPosition);
            if (distance < 0.01f)
            {
                treasureHud.transform.position = screenPosition;
                canScale = false;
                //?scaleCallback();
                popupAnimator.SetTrigger("show");
            }
        }
        /*if(treasureHud.gameObject.activeSelf)
        {
            treasureHud.transform.LookAt(Camera.main.transform);
        }*/
    }
    public void openBox()
    {
        
    }
    public void showDefault()
    {
        tresure.SetActive(true);
    }
    public void showTresure(int balance)
    {
        hideCube(()=> { showTresuerObject(balance); });
    }
    void showTresuerObject(int balance)
    {
        //rewardHud.SetActive(true);
        treasureHud.GetComponentInChildren<RTLTMPro.RTLTextMeshPro3D>().text = SceneLogic.instance.balanceToString(balance);

        tresure.SetActive(true);
        SceneLogic.instance.defaultCallback(0.5f);

        canMove = false;
        treasureHud.gameObject.SetActive(false);
        treasureHud.gameObject.SetActive(true);

        /*var scale = treasureHud.transform.rotation.eulerAngles;
        scale.x += this.transform.rotation.eulerAngles.x;
        scale.y += this.transform.rotation.eulerAngles.y;
        scale.z += this.transform.rotation.eulerAngles.z;
        treasureHud.transform.rotation = Quaternion.Euler(scale);*/
        //canScale = true;

        //?screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2.0f, Screen.height / 2.0f, 0.8f));
        //?screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2.0f, Screen.height / 2.0f, 1.5f));
        screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2.0f, Screen.height / 2.0f, 2.1f));
        originalTresurePos = treasureHud.transform.position;

        var sprite = danadesh.GetComponent<SpriteRenderer>();
        var color = sprite.color;
        color.a = 0;
        sprite.color = color;



        treasureHud.transform.DOMove(screenPosition, 1.5f, false).SetEase(Ease.OutBounce).OnComplete(() =>
        {
            var originalCashPos = sprite.transform.localPosition;
            var pos = originalCashPos;
            pos.y = pos.y + 0.2f;
            sprite.transform.localPosition = pos;

            sprite.DOFade(1, 1.0f).OnComplete(returnToOriginalPos);
            sprite.transform.DOLocalMoveY(originalCashPos.y, 0.5f);
        });
    }
    void returnToOriginalPos()
    {
        var delay = 3;
        var sprite = danadesh.GetComponent<SpriteRenderer>();
        sprite.DOFade(0, 0.5f).SetDelay(delay);
        treasureHud.transform.DOMove(originalTresurePos, 1.0f, false).SetEase(Ease.OutExpo).SetDelay(delay);
    }
    async void scaleCallback()
    {
        await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(4.0f));
        canMove = true;
        popupAnimator.SetTrigger("hide");
    }
    public void showBomb()
    {
        hideCube(showBombObject, false);
    }
    void showBombObject()
    {
        mine.SetActive(true);
        SceneLogic.instance.defaultCallback(2.0f);
    }
    public void hideCube(System.Action callback = null, bool withDelay = true)
    {
        /*if (withDelay)
        {
            //float random = Random.Range(0.3f, 1.5f);
            float random = Random.Range(0.5f, 1.5f);
            await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(random));
        }*/
        if (cubeAnimationHelper == null)
            return;
        //var animator = cubeAnimationHelper.GetComponent<Animator>();
        var hover = cubeAnimationHelper.GetComponentInChildren<BoxHoverEffect>(true);
        if (hover != null)
        {
            hover.enabled = false;
        }
        var animator = cubeAnimationHelper.GetComponentInChildren<Animator>(true);
        if(animator == null)
        {
            var audioSource = this.GetComponent<AudioSource>();
            if (audioSource != null)
                audioSource.Play();

            cubeAnimationHelper.gameObject.SetActive(false);
            callback?.Invoke();

            return;
        }
        if (!animator.enabled)
        {
            var audioSource = this.GetComponent<AudioSource>();
            if(audioSource != null)
                audioSource.Play();

            var box = animator.GetComponent<Box>();
            if (box != null)
            {
                box.onBoxAnimationFinished = () =>
                {
                    cubeAnimationHelper.gameObject.SetActive(false);
                    callback?.Invoke();
                };
                animator.enabled = true;
            }
            else
            {
                cubeAnimationHelper.gameObject.SetActive(false);
                callback?.Invoke();
            }
            //cubeAnimationHelper.GetComponent<CubeAnimation>().onCubeHide = callback;
            
        }
        else
        {
            cubeAnimationHelper.gameObject.SetActive(false);
            Debug.Log("cube already hide ");
            SceneLogic.instance.defaultCallback();
        }
    }
    public void showNormal(int balance, System.Action callback = null)
    {
        //?hideCube(showQuestionMarkIcon, false);
        hideCube(async () =>
        {
            showQuestionMarkIcon();
            Debug.Log("QQQQQQQQQQQQQQQQQQQQQQQ");
            await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(2));
            Debug.Log("CCCCCCCCCCCCCCCCC");
            callback?.Invoke();
        }, false);
    }
    public void showQuestionMarkIcon()
    {
        questionMark.SetActive(true);
    }
    public void showQuestionMark(int balance, System.Action callback)
    {
        //rewardHud.SetActive(true);
        
        questionHud.text = SceneLogic.instance.balanceToString(balance);
        //questionMark.SetActive(true);
        SceneLogic.instance.callbackAfterDelay(1, callback);

        /*questionHud.gameObject.SetActive(false);
        await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(0.5f));
        questionHud.gameObject.SetActive(true);*/
    }
    public void mineFinishedAnim()
    {
        Debug.Log("mine finished anim");
    }
}
