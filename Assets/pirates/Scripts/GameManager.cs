﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class GameManager : MonoBehaviour
{
    public long lastReadActionId = -1;
    public List<Zilal.Action> actionsQueue = new List<Zilal.Action>();
    public int totalTime = 120;
    public string userName = "";
    //public int lastExcutedActionId = -1;
    //public int lastReadIndex;
    List<string> messagesList = new List<string>();
    public static GameManager instance;
    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        Application.logMessageReceivedThreaded += messageReceived;
    }
    void messageReceived(string condition, string stackTrace, LogType type)
    {
        messagesList.Add(condition + " " + System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff tt"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnApplicationQuit()
    {
    }
    private void OnDisable()
    {
        //var log_path = Application.persistentDataPath + "/Player.log";
        var path = System.IO.Path.Combine(Application.persistentDataPath, "log_" + System.DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".txt");
        File.WriteAllLines(path, messagesList);

        var jsonFile = Application.streamingAssetsPath + "/data.json";
        try
        {
            var jsonData = File.ReadAllText(jsonFile);
            File.AppendAllText(path, jsonData);
        }
        catch
        {
            Debug.Log("### exception read data file");
        }
    }
}
