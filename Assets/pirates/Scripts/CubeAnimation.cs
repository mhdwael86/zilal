﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeAnimation : MonoBehaviour
{
    public System.Action onCubeHide;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void cubeHide()
    {
        this.transform.gameObject.SetActive(false);
        onCubeHide?.Invoke();
    }
}
