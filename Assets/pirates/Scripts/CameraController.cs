﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;
    private Animator animator;
    public AudioClip cameraMove;
    public AudioClip cameraBack;
    public AudioSource audioSource;

    public Transform topTransform;
    public Transform boardTransform;
    public Transform questionTransform;
    public Transform carBoxTransform;
    public Transform carTransform;

    public CameraRotator cameraOrbitRotator;
    public Camera camera;


    private void Awake()
    {
        instance = this;
        animator = this.GetComponent<Animator>();
        animator.enabled = false;

        /*camera.transform.position = topTransform.position;
        camera.transform.rotation = topTransform.rotation;
        camera.fieldOfView = 30;*/
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void goToQuestion()
    {
        Debug.Log("move camera to question");
        audioSource.clip = cameraMove;
        audioSource.Play();

        //animator.enabled = true;
        //animator.SetTrigger("toQuestion");

        float duration = 0.5f;
        //?camera.DOFieldOfView(30, duration);
        camera.transform.DOLocalMove(questionTransform.localPosition, duration).OnComplete(() =>
        {
            camera.transform.localPosition = questionTransform.localPosition;
        });
        camera.transform.DOLocalRotate(questionTransform.localRotation.eulerAngles, duration).OnComplete(() =>
        {
            camera.transform.localRotation = questionTransform.localRotation;
            onAnimationFinished();
        });
    }
    public void goToBoard()
    {
        Debug.Log("move camera to board");
        audioSource.clip = cameraBack;
        audioSource.Play();

        //animator.enabled = true;
        //animator.SetTrigger("toBoard");

        float duration = 0.5f;
        //camera.DOFieldOfView(60, duration);
        camera.transform.DOLocalMove(boardTransform.localPosition, duration).OnComplete(() =>
        {
            camera.transform.localPosition = boardTransform.localPosition;
        });
        camera.transform.DOLocalRotate(boardTransform.localRotation.eulerAngles, duration).OnComplete(() =>
        {
            camera.transform.localRotation = boardTransform.localRotation;
            onAnimationFinished();
        });
    }

    public void goToCarBox(Action callback = null)
    {
        float duration = 1f;
        //cameraOrbitRotator.transform.DORotate(Vector3.zero, duration);
        //camera.DOFieldOfView(30, duration);
        camera.transform.DOLocalMove(carBoxTransform.localPosition, duration).OnComplete(() =>
        {
            camera.transform.localPosition = carBoxTransform.localPosition;
        });
        camera.transform.DOLocalRotate(carBoxTransform.localRotation.eulerAngles, duration).OnComplete(() =>
        {
            camera.transform.localRotation = carBoxTransform.localRotation;
            //cameraOrbitRotator.enabled = true;
            callback?.Invoke();
        });
    }
    public void goToCar(Action callback = null)
    {
        float duration = 1f;
        camera.transform.DOLocalMove(carTransform.localPosition, duration).OnComplete(() =>
        {
            camera.transform.localPosition = carTransform.localPosition;
        });
        camera.transform.DOLocalRotate(carTransform.localRotation.eulerAngles, duration).OnComplete(() =>
        {
            camera.transform.localRotation = carTransform.localRotation;
            //cameraOrbitRotator.enabled = true;
            callback?.Invoke();
        });
    }

    public void onAnimationFinished()
    {
        Debug.Log("camera controller onAnimationFinished");
        SceneLogic.instance.defaultCallback(0);
    }

    internal void resertCamera()
    {
        //animator.enabled = true;
        //animator.SetTrigger("idle");
        camera.transform.localPosition = boardTransform.localPosition;
        camera.transform.localRotation = boardTransform.localRotation;
    }
    public void fromTopToBoard(Action callback = null)
    {
        cameraOrbitRotator.enabled = false;
        float duration = 1f;
        cameraOrbitRotator.transform.DORotate(Vector3.zero, duration);
        camera.DOFieldOfView(60, duration);
        camera.transform.DOLocalMove(boardTransform.localPosition, duration).OnComplete(() =>
        {
            camera.transform.localPosition = boardTransform.localPosition;
        });
        camera.transform.DOLocalRotate(boardTransform.localRotation.eulerAngles, duration).OnComplete(() =>
        {
            camera.transform.localRotation = boardTransform.localRotation;
            callback?.Invoke();
        });
    }
    public void fromBoardToTop(Action callback = null)
    {
        //cameraOrbitRotator.enabled = false;
        float duration = 1f;
        //cameraOrbitRotator.transform.DORotate(Vector3.zero, duration);
        camera.DOFieldOfView(60, duration);
        camera.transform.DOLocalMove(topTransform.localPosition, duration).OnComplete(() =>
        {
            camera.transform.localPosition = topTransform.localPosition;
        });
        camera.transform.DOLocalRotate(topTransform.localRotation.eulerAngles, duration).OnComplete(() =>
        {
            camera.transform.localRotation = topTransform.localRotation;
            //cameraOrbitRotator.enabled = true;
            callback?.Invoke();
        });
    }

    public void initialPos()
    {
        setToTop();
        //setToBoard();

        cameraOrbitRotator.enabled = true;
    }
    void setToTop()
    {
        camera.transform.position = topTransform.position;
        camera.transform.rotation = topTransform.rotation;
        //camera.fieldOfView = 30;
    }
    void setToBoard()
    {
        camera.transform.position = boardTransform.position;
        camera.transform.rotation = boardTransform.rotation;
        //camera.fieldOfView = 60;
    }
}
