﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultPopup : MonoBehaviour
{
    public Text balance;
    public RTLTMPro.RTLTextMeshPro userName;
    public RTLTMPro.RTLTextMeshPro resultText;

    private int currentBalance;
    private int startBalance;
    private int targetBalance;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void updateUIAnimated(int balance, string username)
    {
        //this.balance.text = SceneLogic.instance.balanceToString(balance);
        startBalance = currentBalance;
        targetBalance = balance;
        this.userName.text = username;
        //if(isWin)
        if (balance > 0)
            this.resultText.text = Zilal.TextKeys.RESULT_WIN;
        else
            this.resultText.text = Zilal.TextKeys.RESULT_LOSE;

        StartCoroutine(startCounter());
    }
    IEnumerator startCounter()
    {
        yield return new WaitForSeconds(0.1f);
        var step = (targetBalance - startBalance) / 10;
        currentBalance += step;
        if(currentBalance >= targetBalance)
        {
            currentBalance = targetBalance;
            this.balance.text = SceneLogic.instance.balanceToString(currentBalance);
            yield break;
        }
        this.balance.text = SceneLogic.instance.balanceToString(currentBalance);
        StartCoroutine(startCounter());
    }
    public void updateUI(int balance, string username)
    {
        targetBalance = balance;
        currentBalance = balance;
        this.balance.text = SceneLogic.instance.balanceToString(balance);
        this.userName.text = username;
        //if(isWin)
        if(balance > 0)
            this.resultText.text = Zilal.TextKeys.RESULT_WIN;
        else
            this.resultText.text = Zilal.TextKeys.RESULT_LOSE;
        /*else
            this.resultText.text = Zilal.TextKeys.RESULT_LOSE;*/
    }
    public void show()
    {
        this.gameObject.SetActive(true);
    }
    public void hide(bool isImmidat = true)
    {
        currentBalance = targetBalance;
        if(isImmidat)
            this.gameObject.SetActive(false);
    }
}
