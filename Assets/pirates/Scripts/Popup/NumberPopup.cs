using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberPopup : MonoBehaviour
{
    public Text balance;
    [SerializeField]
    private List<string> numbers;
    private int currentIndex = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void startAnimation()
    {
        StartCoroutine(startCounter());
    }
    IEnumerator startCounter()
    {
        yield return new WaitForSeconds(0.1f);
        var currentNumber = numbers[currentIndex];
        this.balance.text = currentNumber;
        currentIndex++;
        if (currentIndex >= numbers.Count)
            currentIndex = 0;
        StartCoroutine(startCounter());
    }
    public void stopAnimation(string winNumber)
    {
        StopAllCoroutines();
        this.balance.text = winNumber;
    }
    public void show()
    {
        currentIndex = 0;
        this.gameObject.SetActive(true);
        startAnimation();
    }
    public void hide(bool isImmidat = true)
    {
        if (isImmidat)
            this.gameObject.SetActive(false);
    }
}
