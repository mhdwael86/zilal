﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Video;

public class SceneLogic : MonoBehaviour
{
    public GameObject[] cubes;
    public GameObject cubePrefap;
    private Vector2 dimintions = new Vector2(3, 3);
    public System.Action callback;
    //public Text state;
    //public Text casheValue;
    public QuestionUI questionUI;
    public Transform cameraBoardTransform;
    public Transform cameraQuestionTransform;
    public Hud hud;
    public ResultPopup resultPopup;
    public ZilzalGround zilzalGround;
    public GameObject tresureHudPrefab;
    public Canvas overlayCanvas;
    public GameObject carAnimation;
    public GameObject conffeti;
    public ParticleSystem smokeParticle;
    public GameObject flameParticlesPanel;
    public ParticleSystem moneyParticle;
    public ParticleSystem moneyRain;
    public CarBox car;

    #region Video
    public VideoPlayer videoPlayer;
    public ResultPopup videoPopup;
    #endregion
    #region Number
    public NumberPopup numberPopup;
    #endregion
    [SerializeField]
    private GameObject environment3d;

    public static SceneLogic instance = null;

    private bool isInitilized = false;
    private void Awake()
    {
        instance = this;
        intiailizeUI();
    }
    // Start is called before the first frame update
    void Start()
    {
        createPrefabs();
    }
    private void OnEnable()
    {
        car.onCarShowed += carShowed;
    }
    private void OnDisable()
    {
        car.onCarShowed -= carShowed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F8))
        {
            Debug.Log("Kill switch");
            killSwitch();
        }
    }
    void killSwitch()
    {
        actionFinished();
        var cameraController = Camera.main.GetComponent<CameraController>();
        if (cameraController != null)
            cameraController.resertCamera();
        cameraGoTo(cameraBoardTransform);
    }
    void intiailizeUI()
    {
        questionUI.gameObject.SetActive(false);
        //?Camera.main.transform.localPosition = cameraBoardTransform.transform.localPosition;
        CameraController.instance.initialPos();
        resultPopup.hide();
        videoPopup.hide();
        numberPopup.hide();

        hud.gameObject.SetActive(true);
        hud.intialize();
        moneyParticle?.gameObject.SetActive(false);
        if(car != null)
            car.gameObject.SetActive(false);
    }
    /*void onOpenCarAnimation()
    {
        car.gameObject.SetActive(true);
        conffeti.gameObject.SetActive(true);
        flameParticlesPanel.gameObject.SetActive(true);
       // await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(1));
        //Camera.main.GetComponent<Animator>().SetTrigger("toCar");
        //CameraController.instance.goToCar();
    }*/
    void cameraGoToQuestion()
    {
    }
    void cameraGoTo(Transform transform)
    {
        Camera.main.transform.localPosition = transform.localPosition;
        Camera.main.transform.localRotation = transform.localRotation;
    }

    void createPrefabs()
    {
        for (int i = 0; i < cubes.Length; i++)
        {
            var cube = cubes[i];
            var prefab = Instantiate(cubePrefap, cube.transform);
            prefab.transform.localPosition = Vector3.zero;
            //prefab.transform.localRotation = Quaternion.identity;
            prefab.transform.SetParent(environment3d.transform);
            prefab.GetComponent<CubePrefab>().cubeAnimationHelper.SetActive(true);
            //prefab.GetComponent<CubePrefab>().tresureHudObject = Instantiate(tresureHudPrefab, overlayCanvas.transform);
            cube.transform.SetParent(prefab.GetComponent<CubePrefab>().cubeAnimationHelper.transform);
            cube.transform.localRotation = prefab.transform.localRotation;
            prefab.transform.localRotation = Quaternion.identity;
            cubes[i] = prefab;
            var cubePrefab = prefab.GetComponent<CubePrefab>();
            cubePrefab.number.text = (i + 1).ToString();
            var testMesh = cube.GetComponentInChildren<TextMesh>(true);
            if(testMesh != null)
            {
                testMesh.text = (i + 1).ToString();
            }
        }
    }
    int getIndex(Zilal.Action action)
    {
        return getIndex(action.target.row, action.target.col);
    }
    int getIndex(int row, int col)
    {
        int index = (row - 1) * (int)dimintions.x + (col - 1);
        return index;
    }
    public void intiailize(Zilal.Action action)
    {
        isInitilized = true;
        showState("intiailize");
        if (action != null)
        {
            GameManager.instance.totalTime = action.totalTime;
            //this.totalTime = action.totalTime;
            hud.setTimer(action.totalTime);
        }

        CameraController.instance.fromTopToBoard(() =>
        {
            defaultCallback();
        });
        //?defaultCallback();
    }
    public void setUserInfo(Zilal.Action action)
    {
        GameManager.instance.userName = action.username;
        hud.setName(action.username);
        defaultCallback(0);
    }
    public void shakeCamera(System.Action callback = null, float delay = 1.1f)
    {
        Camera.main.GetComponent<CameraShake>().shake(delay, callback);
    }
    public void showTresure(Zilal.Action action)
    {
        showState("Tresure");
        //hud.setBalance(action.reward.value);
        var index = getIndex(action);
        cubes[index].GetComponent<CubePrefab>().showTresure(action.reward.value);
        //defaultCallback();
    }
    #region Balance
    public void updateBalance(Zilal.Action action)
    {
        hud.setBalance(action.balance);

        if (resultPopup.gameObject.activeSelf)
        {
            moneyRain.gameObject.SetActive(true);
            moneyRain.Play();
            resultPopup.updateUIAnimated(action.balance, GameManager.instance.userName);
            this.GetComponent<AudioSource>().Play();
            //resultPopup.show();
        }
        if (videoPopup.gameObject.activeSelf)
        {
            Debug.Log("SceneLogic:updateBalance " + action.balance);
            videoPopup.updateUIAnimated(action.balance, GameManager.instance.userName);
            this.GetComponent<AudioSource>().Play();
        } 
        defaultCallback(0);
    }

    public void setFrozenBalance(Zilal.Action action)
    {
        hud.setFrozenBalance(action.frozenBalance);
        defaultCallback(0);
    }
    public void changeDoubleBalance(Zilal.Action action)
    {
        hud.setDoubleBalance(action.doubleBalance);
        defaultCallback(0);
    }
    #endregion
    public void winGame(Zilal.Action action)
    {
        if (action.affectedBoxes == null || action.affectedBoxes.Length <= 0)
        {
            startZilzal(action);
            return;
        }
        for (int i = 0; i < action.affectedBoxes.Length; i++)
        {
            int affectedIndex = getIndex(action.affectedBoxes[i].row, action.affectedBoxes[i].col);
            System.Action callback = null;
            if (i == action.affectedBoxes.Length - 1)
                callback = () => { startZilzal(action); };

            cubes[affectedIndex].GetComponent<CubePrefab>().hideCube(callback);
        }
    }
    public void winCar(Zilal.Action action)
    {
        if (action.affectedBoxes == null || action.affectedBoxes.Length <= 0)
        {
            showCar(action);
            return;
        }
        for (int i = 0; i < action.affectedBoxes.Length; i++)
        {
            int affectedIndex = getIndex(action.affectedBoxes[i].row, action.affectedBoxes[i].col);
            System.Action callback = null;
            if (i == action.affectedBoxes.Length - 1)
                callback = () => { showCar(action); };

            cubes[affectedIndex].GetComponent<CubePrefab>().hideCube(callback);
        }
    }
    void showCar(Zilal.Action action)
    {
        defaultCallback(0);
        resultPopup.hide();
        questionUI.gameObject.SetActive(false);
        videoPlayer.gameObject.SetActive(false);
        numberPopup.hide();
        videoPopup.hide();
        
        if(!isInitilized)
        {
            intiailize(null);
        }
        
        car.gameObject.SetActive(true);
        CameraController.instance.goToCarBox(() =>
        {
            car.startAnimation();
        });
        /*await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(0.5f));
        defaultCallback();
        
        conffeti.gameObject.SetActive(true);
        flameParticlesPanel.gameObject.SetActive(true);

        shakeCamera(null, 6);
        CameraController.instance.goToCar(() =>
        {
            car.GetComponent<AudioSource>().Play();
        });*/
    }
    void carShowed()
    {
        CameraController.instance.goToCar(() =>
        {
            car.startCarAnimation();
        });
    }
    void startZilzal(Zilal.Action action)
    {
        //zilzalGround.gameObject.SetActive(true);
        //carAnimation.gameObject.SetActive(false);

        //zilzalGround.playZilzalSFX();
        //await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(1.5f));
        //zilzalGround.playZilzalAnimation();
        Debug.Log("SceneLogic: beforeShake");
        shakeCamera(() => 
        {
            Debug.Log("SceneLogic: afterShake");
            afterWin(action); 
        }, 6.0f);
        CameraController.instance.fromBoardToTop();
        StartCoroutine(showMoneyParticle());
    }
    IEnumerator showMoneyParticle()
    {
        yield return new WaitForSeconds(1);
        moneyParticle.gameObject.SetActive(true);
        moneyParticle.Play();
        moneyRain.gameObject.SetActive(true);
        moneyRain.Play();
        this.GetComponent<AudioSource>().Play();
    }
    void afterWin(Zilal.Action action)
    {
        //zilzalGround.stopZilzalSFX();
        hud.setBalance(action.reward.value);
        showResult(action.reward.value);
    }
    public void loseGame(Zilal.Action action)
    {
        showResult(action.reward.value);
    }
    public void showResult(int balance)
    {
        Debug.Log("ShowResult " + balance);
        if (isInitilized)
        {
            resultPopup.updateUI(balance, GameManager.instance.userName);
            resultPopup.show();
            defaultCallback(0);
        }
        else
        {
            showVideo();
        }
    }
    public void hideResult(int balance)
    {
        if (isInitilized)
        {
            this.GetComponent<AudioSource>().Stop();
            moneyRain.gameObject.SetActive(false);
            resultPopup.hide();
            defaultCallback(0);
        }
        else
        {
            hideVideo();
        }
    }

    public void backToGame()
    {
        resetCamera();
        //defaultCallback(0);
    }
    public void showBomb(Zilal.Action action)
    {
        Debug.Log("bomb");
        var index = getIndex(action);
        cubes[index].GetComponent<CubePrefab>().showBomb();
    }
    public async void showNormal(Zilal.Action action)
    {
        showState("normal-question");
        var index = getIndex(action);
        Debug.Log("#####################index " + index);
        cubes[index].GetComponent<CubePrefab>().showNormal(action.reward.value, ()=>
        {
            showQuestion(action);
        });

        /*if (action.affectedBoxes == null || action.affectedBoxes.Length <= 0)
        {
            await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(10));
            Debug.Log("show question no affected Boxes");
            showQuestion(action);
            return;
        }
        for (int i = 0; i < action.affectedBoxes.Length; i++)
        {
            int affectedIndex = getIndex(action.affectedBoxes[i].row, action.affectedBoxes[i].col);
            System.Action callback = null;
            if (i == action.affectedBoxes.Length - 1)
            {
                Debug.Log("show question callback");
                callback = () => { showQuestion(action); };
            }
            cubes[affectedIndex].GetComponent<CubePrefab>().hideCube(callback);
        }*/
    }
    public async void showQuestion(Zilal.Action action)
    {
        Debug.Log("show question");
        await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(1));

        if (questionUI != null)
        {
            questionUI.populateQuestion(action);
            questionUI.show();
        }
        Camera.main.GetComponent<CameraController>().goToQuestion();
    }
    public void selectAnswer(Zilal.Action action)
    {
        Debug.Log("selectAnswer");
        questionUI.selectAnswer(action.selectedAnswer);

        defaultCallback(0);
        //Invoke("resetCamera", 3);
    }
    void resetCamera()
    {
        CameraController.instance.goToBoard();
        //?Camera.main.GetComponent<CameraController>().goToBoard();
    }
    void showState(string message)
    {
        //this.state.text = message;
    }
    public void defaultCallback(float delay = 2.0f)
    {
        Debug.Log("Queue defaultCallback");
        Invoke("actionFinished", delay);
    }
    void actionFinished()
    {
        Debug.Log("Queue actionFinished");
        callback?.Invoke();
    }
    public async void callbackAfterDelay(float delay, System.Action callback)
    {
        await System.Threading.Tasks.Task.Delay(TimeSpan.FromSeconds(delay));
        callback?.Invoke();
    }
    public string balanceToString(int balance)
    {
        return balance.ToString("N0");
    }
    public void resetApp()
    {
        if(car != null)
            car.gameObject.SetActive(false);

        CameraController.instance.fromBoardToTop(() =>
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        });
        //UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }

    #region Video
    public void showVideo()
    {
        environment3d.gameObject.SetActive(false);
        videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath, "video.mp4");
        videoPlayer.gameObject.SetActive(true);
        videoPlayer.Play();
        videoPopup.updateUI(hud.CurrentBalance, GameManager.instance.userName);
        videoPopup.show();
        hud.gameObject.SetActive(false);
        defaultCallback(0);
    }
    public void hideVideo()
    {
        environment3d.gameObject.SetActive(true);
        videoPopup.hide();
        videoPlayer.gameObject.SetActive(false);
        this.GetComponent<AudioSource>().Stop();
        hud.gameObject.SetActive(true);
        defaultCallback(0);
    }
    #endregion
    #region Number
    public void showNumber()
    {
        numberPopup.show();
        videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath, "video.mp4");
        videoPlayer.gameObject.SetActive(true);
        videoPlayer.Play();
        hud.gameObject.SetActive(false);
        defaultCallback(0);
    }
    public void winNumber(Zilal.Action action)
    {
        numberPopup.stopAnimation(action.number);
        this.GetComponent<AudioSource>().Play();
        defaultCallback(0);
    }
    public void hideNumber()
    {
        numberPopup.hide();
        hud.gameObject.SetActive(true);
        videoPlayer.gameObject.SetActive(false);
        this.GetComponent<AudioSource>().Stop();
        defaultCallback(0);
    }
    #endregion
}
