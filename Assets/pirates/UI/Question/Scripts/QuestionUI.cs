﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionUI : MonoBehaviour
{
    public RTLTMPro.RTLTextMeshPro question;
    public List<RTLTMPro.RTLTextMeshPro> answers;
    //public List<MeshRenderer> answersOverlay;
    public List<Image> answerOverlay;
    public MeshRenderer answerMeshOverlays;

    private Zilal.Action questionAction;
    public Color originalColor;
    public Color answeredColor;
    public Color overlayRightColor;
    public Color overlayWrongColor;
    //public Color wrongColor;
    public Animator[] animators;

    private int playerAnswerIndex = -1;
    private int correctAnswerIndex = -1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void intiailizeUI()
    {
        //answersOverlay.ForEach(obj => obj.gameObject.SetActive(false));
        answers.ForEach(obj => obj.color = originalColor);
        question.color = originalColor;

        if (playerAnswerIndex != -1)
        {
            animators[playerAnswerIndex].SetTrigger("idle");
        }
        if (correctAnswerIndex != -1)
        {
            animators[correctAnswerIndex].SetTrigger("idle");
        }
    }
    public void populateQuestion(Zilal.Action action)
    {
        intiailizeUI();
        questionAction = action;
    }
    public void show()
    {
        var questionText = questionAction.question.text;
        var index = questionText.IndexOf('؟');
        Debug.Log("question mark index " + index);
        if (index != -1)
            questionText = questionText.Insert(index, " ");

        question.text = questionText;
        answers[0].text = questionAction.question.ans1;
        answers[1].text = questionAction.question.ans2;
        answers[2].text = questionAction.question.ans3;

        changeColor(answerMeshOverlays, Color.white, 0);
        changeColor(answerMeshOverlays, Color.white, 1);
        changeColor(answerMeshOverlays, Color.white, 2);
        foreach (var answer in answers)
        {
            answer.color = Color.white;
        }
        foreach (var ovelay in answerOverlay)
        {
            ovelay.gameObject.SetActive(false);
        }

        this.gameObject.SetActive(true);
    }
    public void selectAnswer(int answer)
    {
        correctAnswerIndex = questionAction.question.correctAnswer - 1;
        var correctAnswerText = answers[correctAnswerIndex];
        var currenAnswer = answers[answer - 1];

        playerAnswerIndex = answer - 1;
        if (answer == questionAction.question.correctAnswer)
        {
            animators[playerAnswerIndex].SetTrigger("right");
            this.GetComponent<AudioSource>().Play();

            //?answers[playerAnswerIndex].color = Color.black;

            //?answerOverlay[playerAnswerIndex].color = overlayRightColor;
            //?answerOverlay[playerAnswerIndex].gameObject.SetActive(true);

            changeColor(answerMeshOverlays, overlayRightColor, playerAnswerIndex);
        }
        else
        {
            animators[correctAnswerIndex].SetTrigger("rightIdle");
            animators[playerAnswerIndex].SetTrigger("wrong");

            //?answers[playerAnswerIndex].color = Color.black;
            //?answers[correctAnswerIndex].color = Color.black;

            //?answerOverlay[playerAnswerIndex].color = overlayWrongColor;
            //?answerOverlay[playerAnswerIndex].gameObject.SetActive(true);

            //?answerOverlay[correctAnswerIndex].color = overlayRightColor;
            //?answerOverlay[correctAnswerIndex].gameObject.SetActive(true);

            changeColor(answerMeshOverlays, overlayRightColor, correctAnswerIndex);
            changeColor(answerMeshOverlays, overlayWrongColor, playerAnswerIndex);
        }
    }
    void changeColor(Renderer rend, Color color, int materialIndex)
    {
        if (materialIndex == 0)
            materialIndex = 1;
        else if (materialIndex == 1)
            materialIndex = 2;
        else if (materialIndex == 2)
            materialIndex = 0;
        Debug.Log("MaterialIndex  " + materialIndex);
        //Set the main Color of the Material to green
        rend.materials[materialIndex].shader = Shader.Find("HDRP/Lit");
        rend.materials[materialIndex].SetColor("_BaseColor", color);
    }
}
