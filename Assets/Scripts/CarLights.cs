using UnityEngine;

public class CarLights : MonoBehaviour
{
    public MeshRenderer targetRenderer;
    public int materialIndex;
    public GameObject[] lights;
    public Material[] materials;
    public float[] durations = { .5f, .5f };

    private float timer = 0f;
    private int current = 0;

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer >= durations[current])
        {
            timer = 0f;
            current = (current + 1) % durations.Length;
            UpdateLights();
        }
    }

    public void UpdateLights()
    {
        Material[] mats = (Material[])targetRenderer.materials.Clone();
        mats[materialIndex] = materials[current];
        targetRenderer.materials = mats;

        for (int i = 0; i < lights.Length; i++)
            lights[i].SetActive(current % 2 == 0);
    }
}
