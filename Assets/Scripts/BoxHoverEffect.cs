using UnityEngine;

public class BoxHoverEffect : MonoBehaviour
{
    public bool playing;
    public Transform firstPoint;
    public Transform secondPoint;
    public Transform targetGraphic;
    public Transform otherHoverObjects;
    public float duration = .5f;
    public float damping = .1f;
    public Vector3 turnSpeed;

    private Vector3 velocity;
    private float timer;
    private float sign = 1f;
    private Vector3 originalPosition;
    private Vector3 otherHoverObjectsOriginalPosition;
    private void Awake()
    {
        originalPosition = targetGraphic.position;
        if(otherHoverObjects != null)
            otherHoverObjectsOriginalPosition = otherHoverObjects.position;
    }

    private void Update()
    {
        if (!playing)
            return;

        targetGraphic.position = Vector3.SmoothDamp(targetGraphic.position, Vector3.Lerp(firstPoint.position, secondPoint.position, timer / duration), ref velocity, Time.deltaTime);
        targetGraphic.Rotate(turnSpeed * Time.deltaTime);

        if(otherHoverObjects != null)
            otherHoverObjects.position = Vector3.SmoothDamp(otherHoverObjects.position, Vector3.Lerp(firstPoint.position, secondPoint.position, timer / duration), ref velocity, Time.deltaTime);

        timer += Time.deltaTime * sign;
        if ((timer >= duration && sign > 0f) || (timer <= 0f && sign < 0f))
            sign = -sign;
    }

    public void ResetPosition()
    {
        targetGraphic.position = originalPosition;
        if(otherHoverObjects != null)
            otherHoverObjects.position = otherHoverObjectsOriginalPosition;
    }
}
