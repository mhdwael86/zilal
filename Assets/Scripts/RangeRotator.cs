using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeRotator : MonoBehaviour
{
	public Vector3 rotationVector;
	public float minRotateSpeed;
	public float maxRotateSpeed;
	public float minAngle;
	public float maxAngle;
	public bool local;
	public bool randomizeDuringGameplay = false;
	public bool handleWithParent = false;

	private float rotateDirection = 1f;
	private float rotateSpeed;
	private float currentAngle = 0f;
	private Quaternion originalRotation;
	private float curMinAngle;
	private float curMaxAngle;

	private void Start()
	{
		rotateSpeed = Random.Range(minRotateSpeed, maxRotateSpeed);
		originalRotation = transform.rotation;
		curMinAngle = minAngle;
		curMaxAngle = maxAngle;
	}

	private void Update()
	{
		if (randomizeDuringGameplay){
			rotateSpeed = Random.Range(minRotateSpeed, maxRotateSpeed);
			curMinAngle = Random.Range(minAngle - 2.5f, minAngle + 2.5f);
			curMaxAngle = Random.Range(maxAngle - 2.5f, maxAngle + 2.5f);
		}
		currentAngle += rotateDirection * rotateSpeed * Time.deltaTime;
		if (currentAngle > curMaxAngle)
			rotateDirection = -1f;
		if (currentAngle < curMinAngle)
			rotateDirection = 1f;
		Vector3 rotVector = local ? transform.InverseTransformDirection(rotationVector) : rotationVector;
		transform.rotation = Quaternion.AngleAxis(currentAngle, rotVector) * originalRotation;
		if (handleWithParent)
			transform.rotation = transform.rotation * transform.parent.rotation;
	}
}
