﻿using UnityEngine;

public class CookiesEffect : MonoBehaviour
{
    public Vector3 minRotationSpeed = new Vector3(0f, 150f, 0f);
    public Vector3 maxRotationSpeed = new Vector3(0f, 210f, 0f);
    public AnimationCurve rotationCurve;
    public float movePossibility = .2f;
    public float moveOffset = .25f;
    public float moveDuration = .5f;
    public bool local = false;
    public bool handlePos = true;

    private Vector3 rotationSpeed;
    private Vector3 currentPos;
    private Vector3 targetPos;
    private float direction = 1f;
    private float timer = 0f;

    private void Start()
    {
	currentPos = targetPos = transform.position;
        if (Random.value < movePossibility)
            targetPos = currentPos + Vector3.up * moveOffset * Mathf.Sign(Random.value - .5f);

	float x = Random.Range(minRotationSpeed.x, maxRotationSpeed.x);
	float y = Random.Range(minRotationSpeed.y, maxRotationSpeed.y);
	float z = Random.Range(minRotationSpeed.z, maxRotationSpeed.z);
	rotationSpeed = new Vector3(x, y, z);
    }

    private void Update()
    {
        timer += Time.deltaTime * direction;
        if (timer > moveDuration && direction > 0f)
            direction = -1f;
        else if (timer < 0f && direction < 0f)
            direction = 1f;
	
	if (handlePos)
        transform.position = Vector3.Lerp(currentPos, targetPos, timer / moveDuration);
	Vector3 rotSpeed = !local ? transform.InverseTransformDirection(rotationSpeed) : rotationSpeed;
        transform.Rotate(rotSpeed * Time.deltaTime * rotationCurve.Evaluate(Time.time * .5f));
    }
}