﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Randomizer : MonoBehaviour
{
    public float radius = 5f;
    public Transform[] objects;

    [ContextMenu("Randomze")]
    public void Randomize()
    {
        for (int i = 0; i < objects.Length; i++)
            objects[i].position = transform.position + new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)).normalized * radius;
    }
}
