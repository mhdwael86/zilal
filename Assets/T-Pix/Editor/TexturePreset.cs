﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace TheoryTeam.TPix
{
    internal class TexturePreset : ScriptableObject
    {
        [Serializable]
        public struct TextureProperties
        {
            [Serializable]
            public struct InputTex
            {
                public string extention;
                public TextureChannel channel;
                public bool invert;
                public bool enabled;
            }

            public string name;
            public InputTex red;
            public InputTex green;
            public InputTex blue;
            public InputTex alpha;
        }

        public List<TextureProperties> textures = new List<TextureProperties>();
    }

    [CustomEditor(typeof(TexturePreset))]
    internal class TexturePresetEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawTexturePresetGUI((TexturePreset)target, EditorGUIUtility.currentViewWidth);
        }

        public static void DrawTexturePresetGUI(TexturePreset target, float width)
        {
            for (int i = 0; i < target.textures.Count; i++)
            {
                EditorGUI.BeginChangeCheck();
                int l = target.textures.Count;
                var temp = DrawTextureProperty(target.textures[i], i);

                if (EditorGUI.EndChangeCheck() && l == target.textures.Count)
                {
                    Undo.RecordObject(target, $"Modfy texture preset {i}");
                    target.textures[i] = temp;
                }
            }

            if (GUILayout.Button("Add Texture"))
            {
                Undo.RecordObject(target, "Add new texture");
                target.textures.Add(new TexturePreset.TextureProperties());
            }

            TexturePreset.TextureProperties.InputTex DrawInputTexture(TexturePreset.TextureProperties.InputTex it, string name)
            {
                EditorGUILayout.BeginHorizontal();
                it.enabled = EditorGUILayout.Toggle((bool)it.enabled, GUILayout.Width(EditorGUIUtility.singleLineHeight));

                GUI.enabled = it.enabled;
                it.extention = EditorGUILayout.TextField(name + " Extention", (string)it.extention);
                it.channel = (TextureChannel)EditorGUILayout.EnumPopup((Enum)it.channel, GUILayout.Width(width * .1f));
                GUI.enabled = true;

                it.invert = GUILayout.Toggle((bool)it.invert, "Invert", EditorStyles.miniButton, GUILayout.Width(width * .15f));
                EditorGUILayout.EndHorizontal();
                return it;
            }

            TexturePreset.TextureProperties DrawTextureProperty(TexturePreset.TextureProperties texProp, int index)
            {
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.BeginHorizontal();
                texProp.name = EditorGUILayout.TextField("Output Name", (string)texProp.name);

                GUI.color = Color.red;
                if (GUILayout.Button("X", EditorStyles.miniButton, GUILayout.Width(EditorGUIUtility.singleLineHeight * 2)))
                    target.textures.RemoveAt(index);
                GUI.color = Color.white;

                EditorGUILayout.EndHorizontal();

                texProp.red = DrawInputTexture(texProp.red, "Red");
                texProp.green = DrawInputTexture(texProp.green, "Green");
                texProp.blue = DrawInputTexture(texProp.blue, "Blue");
                texProp.alpha = DrawInputTexture(texProp.alpha, "Alpha");
                EditorGUILayout.EndVertical();
                return texProp;
            }
        }
    }
}