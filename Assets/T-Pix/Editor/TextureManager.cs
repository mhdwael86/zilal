﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;
using System;

namespace TheoryTeam.TPix
{
    internal enum TextureChannel { R, G, B, A }

    internal class TextureManager : EditorWindow
    {
        [Serializable]
        public struct TextureProperties
        {
            public Texture2D texture;
            public TextureChannel channel;
            public bool invert;
            public bool enabled;

            public TextureProperties(Texture2D texture, TextureChannel channel, bool invert, bool enabled) : this()
            {
                this.texture = texture;
                this.channel = channel;
                this.invert = invert;
                this.enabled = enabled;
            }

            public float[] Get(int size)
            {
                bool inv = invert;
                if (!enabled)
                    return (from v in new float[size] select (inv ? 0f : 1f)).ToArray();

                if (channel == TextureChannel.R)
                    return (from c in texture.GetPixels() select (inv ? 1 - c.r : c.r)).ToArray();
                else if (channel == TextureChannel.G)
                    return (from c in texture.GetPixels() select (inv ? 1 - c.g : c.g)).ToArray();
                else if (channel == TextureChannel.B)
                    return (from c in texture.GetPixels() select (inv ? 1 - c.b : c.b)).ToArray();
                else
                    return (from c in texture.GetPixels() select (inv ? 1 - c.a : c.a)).ToArray();
            }
        }

        private struct ReadableTexture
        {
            public readonly Texture2D texture;
            public readonly string path;
            public readonly TextureImporter textureImporter;

            public ReadableTexture(Texture2D tex, string p)
            {
                texture = tex;
                path = p;
                textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
            }
        }

        private Vector2 scroll;
        private TextureProperties r, g, b, a;
        private TexturePreset preset;
        private bool advancedMode = false;
        private bool editPreset = false;
        private bool deleteUsedTextures = false;
        private int width = 1024;
        private int height = 1024;
        private string path;

        private int Size => width * height;

        [MenuItem("T-Pix/Texture Manager")]
        public static void Display()
        {
            GetWindow<TextureManager>().minSize = new Vector2(400, EditorGUIUtility.singleLineHeight * 13);
            GetWindow<TextureManager>().titleContent = new GUIContent("Texture Manager");
            GetWindow<TextureManager>().Show();
        }

        private void OnGUI()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            GUILayout.FlexibleSpace();

            if (advancedMode)
                deleteUsedTextures = GUILayout.Toggle(deleteUsedTextures, "Delete Used Textures", EditorStyles.toolbarButton);
            advancedMode = GUILayout.Toggle(advancedMode, "Advanced", EditorStyles.toolbarButton);
            EditorGUILayout.EndHorizontal();

            scroll = EditorGUILayout.BeginScrollView(scroll);
            if (!advancedMode)
                DrawSimpleMode();
            else
                DrawAdvancedMode();
            EditorGUILayout.EndScrollView();
        }

        private void DrawSimpleMode()
        {
            GUI.backgroundColor = Color.gray;
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            GUI.backgroundColor = Color.white;
            GUILayout.Label("Texture Coordinate:", EditorStyles.boldLabel);
            GUILayout.FlexibleSpace();
            EditorGUIUtility.labelWidth *= .3333333f;
            width = EditorGUILayout.IntField("Width", width);
            GUILayout.Space(position.width * .1f);
            height = EditorGUILayout.IntField("Height", height);
            EditorGUIUtility.labelWidth *= 3f;
            EditorGUILayout.EndHorizontal();

            r = DrawTexture(r, "Red");
            g = DrawTexture(g, "Green");
            b = DrawTexture(b, "Blue");
            a = DrawTexture(a, "Alpha");

            bool matched = true;
            Texture2D[] textures = new Texture2D[] { r.texture, g.texture, b.texture, a.texture };
            for (int i = 0; i < textures.Length; i++)
                if (textures[i] != null && (textures[i].width != width || textures[i].height != height))
                    matched = false;

            if (!matched)
                EditorGUILayout.HelpBox("One or more texure sizes does not match target texture coordinate you wish to create", MessageType.Error);
            GUI.enabled = matched;

            if (GUI.Button(new Rect(4, position.height - EditorGUIUtility.singleLineHeight * 3 - 8, position.width - 8, EditorGUIUtility.singleLineHeight * 2), "Generate Texture From RGBA Channels"))
            {
                string path = EditorUtility.SaveFilePanelInProject("Save Texture", "New Texture", "png", "");
                Texture2D tex = new Texture2D(width, height);
                ShapeTexture(r.Get(Size), g.Get(Size), b.Get(Size), a.Get(Size), tex);
                byte[] bytes = tex.EncodeToPNG();
                File.WriteAllBytes(path, bytes);
                AssetDatabase.Refresh();
            }

            GUI.enabled = true;
        }

        private void DrawAdvancedMode()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUIUtility.labelWidth *= .3333333f;
            path = EditorGUILayout.TextField("Path", path);

            if (GUILayout.Button("Browse", EditorStyles.miniButton, GUILayout.Width(position.width * .15f)))
                path = EditorUtility.OpenFolderPanel("Textures Base Folder", "Assets", "Assets");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            preset = (TexturePreset)EditorGUILayout.ObjectField("Preset", preset, typeof(TexturePreset), true);
            EditorGUIUtility.labelWidth *= 3f;

            if (GUILayout.Button("New", EditorStyles.miniButton, GUILayout.Width(position.width * .1f)))
            {
                preset = CreateInstance<TexturePreset>();
                AssetDatabase.CreateAsset(preset, "Assets/New Texture Preset.asset");
                AssetDatabase.Refresh();
            }

            editPreset = GUILayout.Toggle(editPreset, "Edit", EditorStyles.miniButton, GUILayout.Width(position.width * .1f));
            EditorGUILayout.EndHorizontal();

            if (editPreset)
            {
                if (preset != null)
                {
                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                    GUILayout.Label("Texture Preset Properties", EditorStyles.boldLabel);
                    TexturePresetEditor.DrawTexturePresetGUI(preset, position.width);
                    EditorGUILayout.EndVertical();
                }
                else
                {
                    EditorGUILayout.HelpBox("No preset assigned!", MessageType.Error);
                }
            }

            if (GUI.Button(new Rect(4, position.height - EditorGUIUtility.singleLineHeight * 3 - 8, position.width - 8, EditorGUIUtility.singleLineHeight * 2), "Apply On All Subfolders"))
                ProcessDirectory(path);
        }

        private void ProcessDirectory(string fullPath)
        {
            HashSet<ReadableTexture> textures = new HashSet<ReadableTexture>();
            string currentPath;

            foreach (string s in Directory.GetFiles(fullPath))
            {
                currentPath = s.Remove(0, s.IndexOf("Assets"));
                Texture2D t = AssetDatabase.LoadAssetAtPath<Texture2D>(currentPath);
                if (t != null)
                    textures.Add(new ReadableTexture(t, currentPath));
            }

            if (textures.Count > 0)
                ProcessTextures(textures.ToArray(), fullPath);

            foreach (string s in Directory.GetDirectories(fullPath))
                ProcessDirectory(s);
        }

        private void ProcessTextures(ReadableTexture[] textures, string path)
        {
            TextureProperties redProp, greenProp, blueProp, alphaProp;
            Texture2D result;
            int r, g, b, a, texSize;
            string baseName;
            HashSet<int> used = new HashSet<int>();

            foreach (ReadableTexture texture in textures)
            {
                texture.textureImporter.isReadable = true;
                texture.textureImporter.SaveAndReimport();
            }

            bool running = true;
            HashSet<int> tempUsed = new HashSet<int>();

            while (running)
            {
                running = false;
                tempUsed.Clear();
                for (int i = 0; i < preset.textures.Count; i++)
                {
                    r = g = b = a = -1;
                    baseName = "";

                    for (int j = 0; j < textures.Length; j++)
                    {
                        if (used.Contains(j) && baseName == "")
                            continue;

                        if (r == -1 && preset.textures[i].red.enabled && FindChannel(preset.textures[i].red.extention, textures[j].texture.name))
                            r = j;
                        if (g == -1 && preset.textures[i].green.enabled && FindChannel(preset.textures[i].green.extention, textures[j].texture.name))
                            g = j;
                        if (b == -1 && preset.textures[i].blue.enabled && FindChannel(preset.textures[i].blue.extention, textures[j].texture.name))
                            b = j;
                        if (a == -1 && preset.textures[i].alpha.enabled && FindChannel(preset.textures[i].alpha.extention, textures[j].texture.name))
                            a = j;

                        if (!used.Contains(j) && (r == j || g == j || b == j || a == j))
                            tempUsed.Add(j);
                    }

                    if (baseName == "")
                        continue;

                    if (r != -1)
                        result = new Texture2D(textures[r].texture.width, textures[r].texture.height);
                    else if (g != -1)
                        result = new Texture2D(textures[g].texture.width, textures[g].texture.height);
                    else if (b != -1)
                        result = new Texture2D(textures[b].texture.width, textures[b].texture.height);
                    else
                        result = new Texture2D(textures[a].texture.width, textures[a].texture.height);

                    result.name = baseName;
                    texSize = result.width * result.height;
                    redProp = new TextureProperties(r == -1 ? null : textures[r].texture, preset.textures[i].red.channel, preset.textures[i].red.invert, preset.textures[i].red.enabled && r != -1);
                    greenProp = new TextureProperties(g == -1 ? null : textures[g].texture, preset.textures[i].green.channel, preset.textures[i].green.invert, preset.textures[i].green.enabled && g != -1);
                    blueProp = new TextureProperties(b == -1 ? null : textures[b].texture, preset.textures[i].blue.channel, preset.textures[i].blue.invert, preset.textures[i].blue.enabled && b != -1);
                    alphaProp = new TextureProperties(a == -1 ? null : textures[a].texture, preset.textures[i].alpha.channel, preset.textures[i].alpha.invert, preset.textures[i].alpha.enabled && a != -1);
                    ShapeTexture(redProp.Get(texSize), greenProp.Get(texSize), blueProp.Get(texSize), alphaProp.Get(texSize), result);
                    byte[] bytes = result.EncodeToPNG();
                    File.WriteAllBytes(path + "/" + baseName + preset.textures[i].name + ".png", bytes);
                    running = true;
                }

                foreach (int item in tempUsed)
                    used.Add(item);
            }

            foreach (ReadableTexture texture in textures)
            {
                texture.textureImporter.isReadable = false;
                texture.textureImporter.SaveAndReimport();
            }

            if (deleteUsedTextures)
            {
                foreach (int i in used)
                {
                    string currentPath = path + '/' + textures[i].texture.name + textures[i].path.Substring((textures[i].path.LastIndexOf('.')));
                    File.Delete(currentPath);
                    File.Delete(currentPath + ".meta");
                }
            }

            AssetDatabase.Refresh();

            bool FindChannel(string extention, string textureName)
            {
                if (textureName.Contains(extention))
                {
                    if (baseName == "")
                    {
                        baseName = textureName.Remove(textureName.IndexOf(extention), extention.Length);
                        return true;
                    }
                    else
                    {
                        if (textureName == baseName + extention)
                            return true;
                    }
                }

                return false;
            }
        }

        private TextureProperties DrawTexture(TextureProperties tex, string text)
        {
            TextureProperties result = tex;
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            result.enabled = EditorGUILayout.Toggle(result.enabled, GUILayout.Width(EditorGUIUtility.singleLineHeight));

            GUI.enabled = result.enabled;
            GUILayout.Label(text, GUILayout.Width(position.width * .2f));
            result.texture = (Texture2D)EditorGUILayout.ObjectField(result.texture, typeof(Texture2D), true);
            result.channel = (TextureChannel)EditorGUILayout.EnumPopup(result.channel, GUILayout.Width(position.width * .2f));
            GUI.enabled = true;

            result.invert = GUILayout.Toggle(result.invert, "Invert", EditorStyles.miniButton);
            EditorGUILayout.EndHorizontal();
            return result;
        }

        private void ShapeTexture(float[] r, float[] g, float[] b, float[] a, Texture2D outTex)
        {
            Color[] pixels = new Color[outTex.width * outTex.height];
            for (int i = 0; i < pixels.Length; i++)
                pixels[i] = new Color(r[i], g[i], b[i], a[i]);
            outTex.SetPixels(pixels);
        }
    }
}