﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;

public class ModelManager : EditorWindow
{
    private string path;

    [MenuItem("T-Pix/Model Manager")]
    public static void Display()
    {
        ModelManager window = GetWindow<ModelManager>();
        window.minSize = new Vector2(400, EditorGUIUtility.singleLineHeight * 4);
        window.titleContent = new GUIContent("Model Manager");
        window.Show();
    }

    private void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();
        path = EditorGUILayout.TextField("Path", path);

        if (GUILayout.Button("Browse", EditorStyles.miniButton, GUILayout.Width(position.width * .15f)))
            path = EditorUtility.OpenFolderPanel("Models Base Folder", "Assets", "Assets");
        EditorGUILayout.EndHorizontal();


        if (GUI.Button(new Rect(4, position.height - EditorGUIUtility.singleLineHeight * 2 - 8, position.width - 8, EditorGUIUtility.singleLineHeight * 2), "Extract All Materials In Subfolders"))
            ProcessDirectory(path);
    }

    private void ProcessDirectory(string path)
    {
        foreach (string p in Directory.GetDirectories(path))
            ProcessDirectory(p);

        string assetPath;
        foreach (string p in Directory.GetFiles(path))
        {
            AssetImporter asset = AssetImporter.GetAtPath(p.Remove(0, p.IndexOf("Assets")));
            if(asset is ModelImporter modelAsset)
            {
                assetPath = p.Remove(0, p.IndexOf("Assets"));
                Directory.CreateDirectory(path + "/Materials");
                string materialsPath = path.Remove(0, path.IndexOf("Assets")) + "/Materials";
                var materials = AssetDatabase.LoadAllAssetsAtPath(modelAsset.assetPath).Where(x => x.GetType() == typeof(Material));
                foreach (Material mat in materials)
                {
                    string tempPath = string.Join(Path.DirectorySeparatorChar.ToString(), new[] { materialsPath, mat.name }) + ".mat";
                    AssetDatabase.ExtractAsset(mat, tempPath);
                    AssetDatabase.WriteImportSettingsIfDirty(tempPath);
                    AssetDatabase.ImportAsset(tempPath, ImportAssetOptions.ForceUpdate);
                }
                AssetDatabase.Refresh();
            }
        }

        AssetDatabase.Refresh();
    }
}
