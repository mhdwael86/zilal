﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;
using System;

namespace TheoryTeam.TPix
{
    internal class MaterialManager : EditorWindow
    {
        [Serializable]
        public struct TexturesGroub
        {
            public string materialsPath;
            public string texturesPath;

            public TexturesGroub(string mp, string tp)
            {
                materialsPath = mp;
                texturesPath = tp;
            }
        }

        List<TexturesGroub> groubs = new List<TexturesGroub>();
        MaterialPreset preset;
        bool editPreset;
        Vector2 scroll;

        [MenuItem("T-Pix/Material Manager")]
        public static void Display()
        {
            MaterialManager window = GetWindow<MaterialManager>();
            window.minSize = new Vector2(400, 300);
            window.titleContent = new GUIContent("Material Manager");
            window.Show();
        }

        void OnGUI()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            GUILayout.FlexibleSpace();
            if(GUILayout.Button("Browse", EditorStyles.toolbarButton))
            {
                string path = EditorUtility.OpenFolderPanel("Main Folder", "Assets/", "");
                if (path != "")
                    FindAllMaterialsGroubs(path);
            }
            EditorGUILayout.EndHorizontal();

            scroll = EditorGUILayout.BeginScrollView(scroll);

            EditorGUILayout.BeginHorizontal();
            preset = (MaterialPreset)EditorGUILayout.ObjectField("Preset", preset, typeof(MaterialPreset), true);

            if (GUILayout.Button("New", EditorStyles.miniButton, GUILayout.Width(position.width * .1f)))
            {
                preset = CreateInstance<MaterialPreset>();
                AssetDatabase.CreateAsset(preset, "Assets/New Material Preset.asset");
                AssetDatabase.Refresh();
            }

            editPreset = GUILayout.Toggle(editPreset, "Edit", EditorStyles.miniButton, GUILayout.Width(position.width * .1f));
            EditorGUILayout.EndHorizontal();

            if (editPreset)
            {
                if (preset != null)
                {
                    GUI.backgroundColor = Color.gray;
                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                    GUI.backgroundColor = Color.white;

                    GUILayout.Label("Material Preset Properties", EditorStyles.boldLabel);
                    MaterialPresetEditor.DrawMaterialPresetGUI(preset);
                    EditorGUILayout.EndVertical();
                }
                else
                {
                    EditorGUILayout.HelpBox("No preset assigned!", MessageType.Error);
                }
            }

            GUI.backgroundColor = Color.gray;
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            GUI.backgroundColor = Color.white;

            GUILayout.Label("Material/Textures Groub", EditorStyles.boldLabel);
            for (int i = 0; i < groubs.Count; i++)
            {
                int count = groubs.Count;
                var v = DrawTextureGroub(i);
                if (count != groubs.Count)
                    break;

                groubs[i] = v;
            }

            if (GUILayout.Button("Add Material/Textures Groub"))
                groubs.Add(new TexturesGroub());

            EditorGUILayout.EndVertical();

            EditorGUILayout.EndScrollView();
            if (GUI.Button(new Rect(4, position.height - EditorGUIUtility.singleLineHeight * 2 - 8, position.width - 8, EditorGUIUtility.singleLineHeight * 2), "Apply On All Materials"))
                AssignTexturesToMaterials();
        }

        void FindAllMaterialsGroubs(string path)
        {
            string[] dirs = Directory.GetDirectories(path);
            string matDir = "";
            string texDir = "";

            for (int i = 0; i < dirs.Length; i++)
            {
                if (dirs[i].ToLower().EndsWith("materials"))
                    matDir = dirs[i];
                if (dirs[i].ToLower().EndsWith("textures"))
                    texDir = dirs[i];
                FindAllMaterialsGroubs(dirs[i]);
            }

            if (matDir != "" && texDir != "")
                groubs.Add(new TexturesGroub(matDir, texDir));
        }

        void AssignTexturesToMaterials()
        {
            Material[] mats;
            Texture2D[] texes;
            Shader targetShader = Shader.Find(preset.shaderName);
            int count = 0;

            foreach (var groub in groubs)
            {
                mats = (from file in Directory.GetFiles(groub.materialsPath) select AssetDatabase.LoadAssetAtPath<Material>(file.Remove(0, file.IndexOf("Assets")))).ToArray();
                mats = (from mat in mats where mat != null select mat).ToArray();
                texes = (from file in Directory.GetFiles(groub.texturesPath) select AssetDatabase.LoadAssetAtPath<Texture2D>(file.Remove(0, file.IndexOf("Assets")))).ToArray();
                texes = (from tex in texes where tex != null select tex).ToArray();

                foreach(Material mat in mats)
                {
                    for (int i = 0; i < texes.Length; i++)
                    {
                        if(texes[i].name.Contains(mat.name))
                        {
                            for(int j = 0; j < preset.properties.Count; j++)
                            {
                                if(texes[i].name.Contains(preset.properties[j].textureExtention))
                                {
                                    if (mat.shader != targetShader)
                                        mat.shader = targetShader;

                                    mat.SetTexture(preset.properties[j].propertyName, texes[i]);
                                    count++;
                                }
                            }
                        }
                    }
                }
            }

            Debug.Log(count + " Textures Assigned!");
            AssetDatabase.Refresh();
        }

        TexturesGroub DrawTextureGroub(int index)
        {
            TexturesGroub groub = groubs[index];
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            groub.materialsPath = EditorGUILayout.TextField("Materials Path", groub.materialsPath);
            groub.texturesPath = EditorGUILayout.TextField("Textures Path", groub.texturesPath);

            GUI.color = Color.red;
            if (GUILayout.Button("X", EditorStyles.miniButton, GUILayout.Width(EditorGUIUtility.singleLineHeight * 2)))
                groubs.RemoveAt(index);
            GUI.color = Color.white;

            EditorGUILayout.EndHorizontal();
            return groub;
        }
    }
}