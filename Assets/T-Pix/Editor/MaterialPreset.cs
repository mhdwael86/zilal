﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace TheoryTeam.TPix
{
    internal class MaterialPreset : ScriptableObject
    {
        [Serializable]
        public struct MaterialProperty
        {
            public string propertyName;
            public string textureExtention;

            public MaterialProperty(string pn, string te)
            {
                propertyName = pn;
                textureExtention = te;
            }
        }

        public string shaderName;
        public List<MaterialProperty> properties = new List<MaterialProperty>();
    }

    [CustomEditor(typeof(MaterialPreset))]
    internal class MaterialPresetEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            GUILayout.Label("Material Properties", EditorStyles.boldLabel);
            DrawMaterialPresetGUI((MaterialPreset)target);
            EditorGUILayout.EndVertical();
        }

        public static void DrawMaterialPresetGUI(MaterialPreset preset)
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            EditorGUI.BeginChangeCheck();
            string temp = EditorGUILayout.TextField("Shader", preset.shaderName);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(preset, "Modify shader name");
                preset.shaderName = temp;
            }
            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < preset.properties.Count; i++)
            {
                EditorGUI.BeginChangeCheck();
                int count = preset.properties.Count;
                var v = DrawMaterialProperty(preset.properties, i);
                bool changed = EditorGUI.EndChangeCheck();
                if (count != preset.properties.Count)
                    break;

                if (changed)
                {
                    Undo.RecordObject(preset, $"Modify material preset {i}");
                    preset.properties[i] = v;
                }
            }

            if (GUILayout.Button("Add Property"))
            {
                Undo.RecordObject(preset, "Add property");
                preset.properties.Add(new MaterialPreset.MaterialProperty());
            }
        }

        public static MaterialPreset.MaterialProperty DrawMaterialProperty(List<MaterialPreset.MaterialProperty> properties, int index)
        {
            var p = properties[index];
            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            EditorGUIUtility.labelWidth *= .75f;
            p.propertyName = EditorGUILayout.TextField("Property Name", p.propertyName);
            p.textureExtention = EditorGUILayout.TextField("Texture Extention", p.textureExtention);
            EditorGUIUtility.labelWidth *= 4f / 3f;

            GUI.color = Color.red;
            if (GUILayout.Button("X", EditorStyles.miniButton, GUILayout.Width(EditorGUIUtility.singleLineHeight * 2)))
                properties.RemoveAt(index);
            GUI.color = Color.white;

            EditorGUILayout.EndHorizontal();
            return p;
        }
    }
}